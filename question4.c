#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<unistd.h>
#define DIM 9
#include <sys/types.h>

#define DIM 9
int values[DIM] = {1, 60, 9, 18, 91, 7, 26, 75, 6};

void print_max()
{
	int max = 0;
	for (int i = 0; i < DIM; i++)
	{
		if (max < values[i])
		{
			max = values[i];
		}
	}
	printf("Maximum is %d\n", max);
}
void print_avg()
{
	int sum = 0;
	for (int i= 0; i <DIM; i++)
	{
		sum += values[i];
	}
	printf("Average is %f\n", (float) (sum) /DIM);
}
void print_values()
{
	printf("Values: ");
	for (int i = 0; i < DIM; i++)
	{
	printf("%d ", values[i]);
	}
	printf("\n");
}
//modifying into multipirocess 
int main()
{
	//declare variables 
	int pid, pid1, pid2;

	//creating child process 
	pid = fork();
	
	//process first child 
	if (pid == 0){
	print_values();
	wait(NULL);
	}
	
	//process second child
	else {
	pid1 = fork();
	if(pid1 == 0){
	print_max();
	wait(NULL);
	}
	
	//process third child
	else {
	pid2 = fork();
	if (pid2 == 0){
	printf("\n");
	print_avg();
	wait(NULL);
	}
	}
	}
	
	return 0;
}
	
	

